<?php
    
require_once '../vendor/autoload.php';

use BitAndBlack\ProcessLog\ProcessWatcher;
use BitAndBlack\ProcessLog\ProcessWriter;
use BitAndBlack\ProcessLog\Status\Message;
use Spatie\Async\Pool;
use Symfony\Component\Console\Output\ConsoleOutput;

$output = new ConsoleOutput();
$output->writeln('Starting');
$output->writeln('Waiting for the threads to finish...');

$pool = Pool::create();
$pool->concurrency(5);

$processesFolder = __DIR__ .DIRECTORY_SEPARATOR.'processes';

foreach (range(10, 25) as $uid) {

    $processWriter = new ProcessWriter($uid, $processesFolder);

    $pool[] = async(static function() use ($uid, $processWriter) 
        {
            $loops = random_int(5, 25);
            $message = new Message('Description of process activity here / <info>UID: '.$uid.'</info>');
            
            for ($counter = 1; $counter < $loops; $counter++) {
                sleep(random_int(1, 3));
                $processWriter->log($message);
            }
            
            return $uid;
        })
        ->then(static function() use ($processWriter) {
            $processWriter->clear();
        })
    ;
}

$processWatcher = new ProcessWatcher($processesFolder, $output);
$processWatcher
    ->setInterval(1000000)
    ->setTimeOut(2000000)
    ->watch()
;

$result = await($pool);

$output->write("Finished all processes\033[K\n");

var_dump($result);
