<?php

/**
 * Bit&Black Process Log.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ProcessLog;

use BitAndBlack\ProcessLog\Status\StatusInterface;

/**
 * Class NullProcessWriter
 * 
 * @package BitAndBlack\ProcessLog
 */
class NullProcessWriter implements ProcessWriterInterface
{
    /**
     * NullProcessWriter constructor.
     * 
     * @param string $uid
     * @param string $folder
     */
    public function __construct(string $uid = '', string $folder = '') 
    {
        unset($uid, $folder);
    }

    /**
     * @param \BitAndBlack\ProcessLog\Status\StatusInterface $status
     * @return bool
     */
    public function log(StatusInterface $status): bool
    {
        return true;
    }

    /**
     * @return bool
     */
    public function clear(): bool
    {
        return true;
    }
}
