<?php

/**
 * Bit&Black Process Log.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ProcessLog;

use BitAndBlack\Helpers\ArrayHelper;
use RuntimeException;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Terminal;

/**
 * Class ProcessWatcher
 * 
 * @package BitAndBlack\ProcessLog
 */
class ProcessWatcher
{
    private string $processesFolder;
    
    private int $interval = 1_000_000;
    
    private OutputInterface $output;
    
    private Terminal $terminal;
    
    private string $replacement;
    
    private int $replacementLength;

    /**
     * AsyncOutput constructor.
     *
     * @param string $processesFolder
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    public function __construct(string $processesFolder, OutputInterface $output)
    {
        $this->processesFolder = $processesFolder;
        $this->output = $output;
        $this->terminal = new Terminal();

        if (!file_exists($this->processesFolder) 
            && !mkdir(
                $concurrentDirectory = $this->processesFolder,
                0777,
                true
            ) 
            && !is_dir($concurrentDirectory)
        ) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }
        
        register_shutdown_function([$this, 'clearAll']);

        $this->replacement = '(...)';
        $this->replacementLength = strlen($this->replacement) + 1;
    }

    /**
     * Sets a timeout. This can be used to wait for the first process files.
     *
     * @param int $microseconds
     * @return \BitAndBlack\ProcessLog\ProcessWatcher
     */
    public function setTimeOut(int $microseconds): self
    {
        usleep($microseconds);
        return $this;
    }

    /**
     * Sets the interval time
     *
     * @param int $microseconds
     * @return \BitAndBlack\ProcessLog\ProcessWatcher
     */
    public function setInterval(int $microseconds): self
    {
        $this->interval = $microseconds;
        return $this;
    }

    /**
     * Watches for files inside a folder
     */
    public function watch(): void
    {
        $lines = 0;
        $processesMax = 0;
        
        while ($files = glob($this->processesFolder.DIRECTORY_SEPARATOR.'*.process')) {
            
            $filesCount = count($files);
            
            if ($filesCount > $processesMax) {
                $processesMax = $filesCount;
            }
            
            $this->output->write($filesCount." processes running...\033[K\n");
            $lines++;
            
            natsort($files);
            
            foreach ($files as $file) {
                if (file_exists($file) && false !== $content = @file_get_contents($file)) {
                    
                    $output = 'Process #'.pathinfo($file, PATHINFO_FILENAME).': '.$content;
                    $outputLength = strlen($output);
                    $terminalWidth = $this->terminal->getWidth();
                    
                    if ($outputLength > $terminalWidth) {
                        $stringShortened = substr($output, 0, $terminalWidth - $this->replacementLength);
                        $output = $stringShortened.'</>'.$this->replacement; 
                    }
                    
                    $this->output->write($output."\033[K".PHP_EOL);
                    ++$lines;
                }
            }
            
            /** removes lines */
            for ($counter = $lines; $counter <= $processesMax; ++$counter) {
                $this->output->write("\033[2K");
            }

            /** goes lines up */
            for ($counter = 0; $counter < $lines; ++$counter) {
                $this->output->write("\033[1A");        
            }
            
            $lines = 0;
            
            usleep($this->interval);
        }
    }

    /**
     * Removes old files in case of shutdown
     */
    public function clearAll(): void
    {
        $files = glob($this->processesFolder.DIRECTORY_SEPARATOR.'*.process');
        
        foreach (ArrayHelper::getArray($files) as $file) {
            if (!is_string($file) || !is_file($file)) {
                continue;
            }
            unlink($file);
        }
    }
}
