<?php

/**
 * Bit&Black Process Log.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ProcessLog\Status;

/**
 * Class Message
 * 
 * @package BitAndBlack\ProcessLog\Status
 */
class Message implements StatusInterface
{
    private string $message;

    /**
     * Message constructor.
     * 
     * @param string $message
     */
    public function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * Returns the current status
     *
     * @return string
     */
    public function getValue(): string
    {
        return $this->message;
    }
}