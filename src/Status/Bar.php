<?php

/**
 * Bit&Black Process Log.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ProcessLog\Status;

/**
 * Class Bar
 * 
 * @package BitAndBlack\ProcessLog\Status
 */
class Bar implements StatusInterface
{
    private int $stepsCurrent = 0;

    private int $stepsAll;

    private int $length;

    /**
     * Bar constructor.
     *
     * @param int $stepsAll
     * @param int $length
     */
    public function __construct(int $stepsAll, int $length = 20)
    {
        $this->stepsAll = $stepsAll;
        $this->length = $length;
    }

    /**
     * Counts one step higher
     * 
     * @param int $steps
     */
    public function advance(int $steps = 1): void
    {
        $this->stepsCurrent += $steps;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getValue();
    }

    /**
     * Returns the current value 
     * 
     * @return string
     */
    public function getValue(): string
    {
        $percent = $this->stepsCurrent / $this->stepsAll;
        $arrow = 1 === $percent ? '' : '>';
        $bar = str_pad('', (int) ($this->length * $percent), '=', STR_PAD_RIGHT);
        
        return str_pad((string) $this->stepsCurrent, strlen((string) $this->stepsAll), ' ', STR_PAD_LEFT).
            ' |'.str_pad($bar.$arrow, $this->length, '-', STR_PAD_RIGHT).'| '.
            $this->stepsAll;
    }
}
