<?php

/**
 * Bit&Black Process Log.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ProcessLog\Status;

/**
 * Interface StatusInterface
 * 
 * @package BitAndBlack\ProcessLog\Status
 */
interface StatusInterface
{
    /**
     * Returns the current status
     * 
     * @return string
     */
    public function getValue(): string;
}