<?php

/**
 * Bit&Black Process Log.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ProcessLog;

use BitAndBlack\ProcessLog\Status\StatusInterface;
use RuntimeException;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\SemaphoreStore;

/**
 * Class ProcessWriter
 * 
 * @package BitAndBlack\ProcessLog
 */
class ProcessWriter implements ProcessWriterInterface
{
    private string $file;
    
    private LockFactory $factory;

    /**
     * ProcessWriter constructor.
     * 
     * @param string $uid
     * @param string $folder
     */
    public function __construct(string $uid, string $folder)
    {
        $this->file = $folder.DIRECTORY_SEPARATOR.$uid.'.process';

        if (!file_exists($folder) && !mkdir($folder, 0777, true) && !is_dir($folder)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $folder));
        }

        $store = new SemaphoreStore();
        $this->factory = new LockFactory($store);
    }

    /**
     * @param \BitAndBlack\ProcessLog\Status\StatusInterface $status
     * @return bool
     */
    public function log(StatusInterface $status): bool
    {
        $lock = $this->factory->createLock($this->file);

        if ($lock->acquire()) {
            if (false !== $file = fopen($this->file, 'wb')) {
                fwrite($file, $status->getValue());
                fclose($file);
            }
            
            $lock->release();
            return true;
        }
        
        return false;
    }

    /**
     * Removes the process file
     * 
     * @return bool
     */
    public function clear(): bool
    {
        if (file_exists($this->file)) {
            return unlink($this->file);
        }
        
        return true;
    }
}
