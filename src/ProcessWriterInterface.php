<?php

/**
 * Bit&Black Process Log.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ProcessLog;

use BitAndBlack\ProcessLog\Status\StatusInterface;

/**
 * Interface ProcessWriterInterface
 * 
 * @package BitAndBlack\ProcessLog
 */
interface ProcessWriterInterface
{
    /**
     * ProcessWriterInterface constructor.
     * 
     * @param string $uid
     * @param string $folder
     */
    public function __construct(string $uid, string $folder);

    /**
     * @param \BitAndBlack\ProcessLog\Status\StatusInterface $status
     * @return bool
     */
    public function log(StatusInterface $status): bool;

    /**
     * @return bool
     */
    public function clear(): bool;
}