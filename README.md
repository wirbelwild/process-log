[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/process-log)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/758e2d37e81643c18ba2fa6c17288045)](https://www.codacy.com/bb/wirbelwild/process-log/dashboard)
[![Total Downloads](https://poser.pugx.org/bitandblack/process-log/downloads)](https://packagist.org/packages/bitandblack/process-log)
[![License](https://poser.pugx.org/bitandblack/process-log/license)](https://packagist.org/packages/bitandblack/process-log)

# Bit&Black Process Log.

Helps logging multiple and possibly asynchronously running processes by writing and reading their statuses in files.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/process-log). Add it to your project by running `$ composer require bitandblack/process-log`. 

## Usage 

### 1. Set up and update a process 

Set up a process writer with a unique id and a folder where all the processes can be stored:

````php
<?php 

use BitAndBlack\ProcessLog\ProcessWriter;

$processWriter = new ProcessWriter(
    'MyUniqueProcessId', 
    '/myProcessesFolder'
);
````

Create a status object and tell it something:

````php
<?php

use BitAndBlack\ProcessLog\Status\Message;

$status = new Message('Here is my status');
````

You can also create a progress bar. Set it up with the total count of steps:

````php
<?php

use BitAndBlack\ProcessLog\Status\Bar;

$status = new Bar(100);
````

For each step in your action store the status with the process writer: 

````php
<?php

foreach ($myAction as $step) {
    /** Do something here... */
    $processWriter->log($status);
}
````

If you have a progress bar you can update the count by calling `$status->advance()`.

At the end clear the process: 

````php
<?php 

$processWriter->clear();
````

### 2. Watch all processes 

To watch the status of your processes you need to use the ProcessWatcher. Set it up with the folder of your processes and with an output object. 

The output object needs to implement the `Symfony\Component\Console\Output\OutputInterface`.

````php
<?php

use BitAndBlack\ProcessLog\ProcessWatcher;

$processWatcher = new ProcessWatcher(
    '/myProcessesFolder', 
    $myOutputObject
);
````

After that, define in which interval you want to refresh the status output, define a initial wait time and start the watcher then. 

The time is in microseconds here: 

````php
<?php

$processWatcher
    ->setInterval(1000000)
    ->setTimeOut(2000000)
    ->watch()
;
````

The watcher will run as long as there are files inside you processes folders and quit after that.

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
