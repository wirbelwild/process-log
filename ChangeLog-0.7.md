# Changes in Bit&Black Process Log v0.7

## 0.7.0 2020-11-27

### Fixed

-   Fixed missing end for color formatting.

### Added

-   Added support for PHP 8.
-   Added PHPUnit