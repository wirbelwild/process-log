# Changes in Bit&Black Process Log v1.0

## 1.0.0 2022-03-10

### Changed

-   PHP >=7.4 is now required.