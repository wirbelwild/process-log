<?php

/**
 * Bit&Black Process Log.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ProcessLog\Test;

use BitAndBlack\ProcessLog\ProcessWatcher;
use BitAndBlack\ProcessLog\ProcessWriter;
use BitAndBlack\ProcessLog\Status\Message;
use PHPUnit\Framework\TestCase;
use Spatie\Async\Pool;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class ProcessWatcherTest. 
 * 
 * @package BitAndBlack\ProcessLog\Test
 */
class ProcessWatcherTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testCanLog(): void 
    {
        $output = new ConsoleOutput();
        $output->writeln('Starting');
        $output->writeln('Waiting for the threads to finish...');

        $pool = Pool::create();
        $pool->concurrency(5);

        $processesFolder = __DIR__.DIRECTORY_SEPARATOR.'processes';

        foreach (range(5, 10) as $uid) {

            $processWriter = new ProcessWriter((string) $uid, $processesFolder);

            $pool[] = async(static function() use ($uid, $processWriter)
                {
                    $loops = random_int(3, 5);
                    $message = new Message('Description of process activity here / <info>UID: '.$uid.'</info>');
    
                    for ($counter = 1; $counter < $loops; ++$counter) {
                        sleep(random_int(1, 3));
                        $processWriter->log($message);
                    }
    
                    return $uid;
                })
                ->then(static function() use ($processWriter) {
                    $processWriter->clear();
                })
            ;
        }

        $processWatcher = new ProcessWatcher($processesFolder, $output);
        $processWatcher
            ->setInterval(1_000_000)
            ->setTimeOut(2_000_000)
            ->watch()
        ;

        $result = await($pool);

        $output->write("Finished all processes\033[K\n");

        rmdir($processesFolder);
        
        self::assertNotEmpty(
            $result
        );
    }
}
